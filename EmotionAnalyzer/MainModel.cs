﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.ProjectOxford.Emotion;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using PropertyChanged;

namespace EmotionAnalyzer
{
    [ImplementPropertyChanged]
    public class MainModel
    {
        private readonly EmotionServiceClient _client = new EmotionServiceClient("d292cc77a9f14eb5b1cd98cae08b2b50");

        public int ProgressMax { get; set; }
        public int ProgressValue { get; set; }
        public string ImagesDir { get; set; } = @"\\Mac\Home\Documents\Cognitive\Frames";

        public PlotModel PlotModel { get; set; }
        public ImageSource Image { get; set; }

        internal void AnalyzeEmotions()
        {
            AnalyzeEmotionsInternal().ContinueWith(task =>
            {
                if (task.Exception != null)
                {
                    MessageBox.Show(task.Exception.ToString(), "Error");
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private async Task AnalyzeEmotionsInternal()
        {
            var files = Directory.GetFiles(ImagesDir);

            Dispatch(() => ProgressMax = files.Length);

            for (int i = 0; i < files.Length; i++)
            {
                Dispatch(() => Image = new BitmapImage(new Uri(files[i], UriKind.Absolute)));

                var stream = new FileStream(files[i], FileMode.Open, FileAccess.Read);
                var result = await _client.RecognizeAsync(stream);

                stream.Close();

                var scores = result[0].Scores.ToRankedList();

                Dispatch(() =>
                {
                    UpdatePlot(scores, i);
                    ProgressValue = i;
                });
                

                await Task.Delay(5000);
            }
        }

        private void UpdatePlot(IEnumerable<KeyValuePair<string, float>> scores, int idx)
        {
            if (PlotModel == null)
            {
                InitPlot(scores);
            }

            foreach (var pair in scores)
            {
                ((LineSeries)PlotModel.Series.First(x => x.Title == pair.Key)).Points.Add(new DataPoint(idx, pair.Value));
            }

            PlotModel.DefaultXAxis.Zoom(0, idx);
            PlotModel.InvalidatePlot(true);
        }

        private void InitPlot(IEnumerable<KeyValuePair<string, float>> scores)
        {
            var model = new PlotModel
                        {
                            Axes =
                            {
                                new LinearAxis {Position = AxisPosition.Bottom},
                                new LinearAxis {Position = AxisPosition.Left, Minimum = 0, Maximum = 1.1d}
                            },
                            IsLegendVisible = true,
                            LegendPosition = LegendPosition.TopRight
                        };

            foreach (var pair in scores)
            {
                model.Series.Add(new LineSeries
                                 {
                                     Title = pair.Key
                                 });
            }

            PlotModel = model;
        }

        private void Dispatch(Action action)
        {
            Dispatcher.CurrentDispatcher.Invoke(action);
        }
    }
}